[![NPM Version](https://img.shields.io/npm/v/tslint-deexclude.svg)](https://www.npmjs.com/package/tslint-deexclude)
[![License](https://img.shields.io/npm/l/tslint-deexclude.svg)](LICENSE)

# tslint-deexclude

(Forked from the tsling-defocus repo from Sergiioo: https://github.com/Sergiioo/tslint-defocus)

## About

This is a tslint rule that warns about focussed Jasmine tests - `xdescribe` and `xit`

## Usage

- Install with: `npm install tslint-deexclude --save-dev` or `yarn add tslint-deexclude --dev`
- Extend this package in your `.tslint.json` file, e.g.:

```
  "extends": [
    "tslint-deexclude"
  ],
  "rules": {
    "deexclude": true,
    ...
```

(as per the [instructions for custom rules](http://palantir.github.io/tslint/usage/custom-rules/))

- Run `tslint` as you usually would (gulp plugin, directly from node, etc)
- If you forget to remove a call to `xdescribe` or `xit` then you will see something like from tslint:

```
(deexclude) app.ts[4, 1]: Calls to 'xdescribe' are not allowed.
(deexclude) app.ts[8, 5]: Calls to 'xit' are not allowed.
```

## Dependencies

Version 2.0.x of this rule requires version 5.x of tslint.

## Developer instructions

- installed the required global npm packages: `npm install gulp --global --no-optional`.
- Clone [from github](https://github.com/Sergiioo/tslint-deexclude)
- Run `npm install` or `yarn install` to install and get started
- This repo uses npm scripts for its build. Try `yarn build` and `yarn test`.
- There are also watch mode variants - `yarn build:watch` and `yarn test:watch`.

## License

MIT
